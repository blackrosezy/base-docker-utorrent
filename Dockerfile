FROM ubuntu:trusty

MAINTAINER Mohd Rozi <blackrosezy@gmail.com>

RUN echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/nginx-stable-$(lsb_release -cs).list

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

RUN apt-get update && DEBIAN_FRONTEND=noninteractive ; apt-get -y install \
    nginx build-essential dos2unix python-pip wget syslog-ng syslog-ng-core \
    && rm -rf /var/lib/apt/lists/*
    
# utserver
# http://download-new.utorrent.com/endpoint/utserver/os/linux-x64-ubuntu-13-04/track/beta/
RUN wget https://www.dropbox.com/s/gr3o9tc8z1zovdw/utserver.tar.gz?dl=1 -O utserver.tar.gz
RUN tar -zxvf utserver.tar.gz -C /opt/
RUN chmod 777 /opt/utorrent-server-alpha-v3_3/
RUN ln -s /opt/utorrent-server-alpha-v3_3/utserver /usr/bin/utserver

RUN pip install supervisor
    
RUN mkdir -p /var/log/supervisor

# Replace the system() source because inside Docker we can't access /proc/kmsg.
# https://groups.google.com/forum/#!topic/docker-user/446yoB0Vx6w
RUN	sed -i -E 's/^(\s*)system\(\);/\1unix-stream("\/dev\/log");/' /etc/syslog-ng/syslog-ng.conf

# Uncomment 'SYSLOGNG_OPTS="--no-caps"' 
RUN	sed -i 's/^#\(SYSLOGNG_OPTS="--no-caps"\)/\1/g' /etc/default/syslog-ng